webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../auth/auth.module": [
		"../../../../../src/app/auth/auth.module.ts",
		"auth.module"
	],
	"../dashboard-accounts/dashboard-accounts.module": [
		"../../../../../src/app/dashboard-accounts/dashboard-accounts.module.ts",
		"dashboard-accounts.module"
	],
	"../forms/forms.module": [
		"../../../../../src/app/forms/forms.module.ts",
		"forms.module"
	],
	"../main/sample/sample.module": [
		"../../../../../src/app/main/sample/sample.module.ts",
		"sample.module"
	],
	"../register/register.module": [
		"../../../../../src/app/register/register.module.ts",
		"register.module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- <div [@routerAnimation]=\"getRouteAnimation(route)\"> -->\r\n  <router-outlet ></router-outlet>\r\n<!-- </div> -->"

/***/ }),

/***/ "../../../../../src/app/app.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent.prototype.getRouteAnimation = function (outlet) {
        return outlet.activatedRouteData.animation;
    };
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.scss")],
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__lazy_load_lazy_load_module__ = __webpack_require__("../../../../../src/app/lazy-load/lazy-load.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser_animations__ = __webpack_require__("../../../platform-browser/esm5/animations.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_core_module__ = __webpack_require__("../../../../../src/app/core/core.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["K" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3__lazy_load_lazy_load_module__["a" /* LazyLoadModule */],
                __WEBPACK_IMPORTED_MODULE_5__core_core_module__["a" /* CoreModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */]
            ],
            providers: [],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/core/core.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CoreModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sidemenu_sidemenu_component__ = __webpack_require__("../../../../../src/app/core/sidemenu/sidemenu.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__sidemenu_item_sidemenu_item_component__ = __webpack_require__("../../../../../src/app/core/sidemenu-item/sidemenu-item.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_material_list__ = __webpack_require__("../../../material/esm5/list.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material_button__ = __webpack_require__("../../../material/esm5/button.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_material_input__ = __webpack_require__("../../../material/esm5/input.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_material_icon__ = __webpack_require__("../../../material/esm5/icon.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_material_toolbar__ = __webpack_require__("../../../material/esm5/toolbar.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_material_chips__ = __webpack_require__("../../../material/esm5/chips.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_material_form_field__ = __webpack_require__("../../../material/esm5/form-field.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_ngx_perfect_scrollbar__ = __webpack_require__("../../../../ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_flex_layout__ = __webpack_require__("../../../flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__toolbar_notification_toolbar_notification_component__ = __webpack_require__("../../../../../src/app/core/toolbar-notification/toolbar-notification.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__toolbar_toolbar_component__ = __webpack_require__("../../../../../src/app/core/toolbar/toolbar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__search_bar_search_bar_component__ = __webpack_require__("../../../../../src/app/core/search-bar/search-bar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__fullscreen_fullscreen_component__ = __webpack_require__("../../../../../src/app/core/fullscreen/fullscreen.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__sidebar_sidebar_component__ = __webpack_require__("../../../../../src/app/core/sidebar/sidebar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__user_menu_user_menu_component__ = __webpack_require__("../../../../../src/app/core/user-menu/user-menu.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};























var DEFAULT_PERFECT_SCROLLBAR_CONFIG = {
    suppressScrollX: true
};
var CoreModule = /** @class */ (function () {
    function CoreModule() {
    }
    CoreModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__sidemenu_sidemenu_component__["a" /* SidemenuComponent */],
                __WEBPACK_IMPORTED_MODULE_3__sidemenu_item_sidemenu_item_component__["a" /* SidemenuItemComponent */],
                __WEBPACK_IMPORTED_MODULE_15__toolbar_notification_toolbar_notification_component__["a" /* ToolbarNotificationComponent */],
                __WEBPACK_IMPORTED_MODULE_16__toolbar_toolbar_component__["a" /* ToolbarComponent */],
                __WEBPACK_IMPORTED_MODULE_17__search_bar_search_bar_component__["a" /* SearchBarComponent */],
                __WEBPACK_IMPORTED_MODULE_18__fullscreen_fullscreen_component__["a" /* FullscreenComponent */],
                __WEBPACK_IMPORTED_MODULE_19__sidebar_sidebar_component__["a" /* SidebarComponent */],
                __WEBPACK_IMPORTED_MODULE_20__user_menu_user_menu_component__["a" /* UserMenuComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material_list__["a" /* MatListModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material_button__["a" /* MatButtonModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material_input__["b" /* MatInputModule */],
                __WEBPACK_IMPORTED_MODULE_7__angular_material_icon__["a" /* MatIconModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_material_chips__["a" /* MatChipsModule */],
                __WEBPACK_IMPORTED_MODULE_12__angular_router__["a" /* RouterModule */],
                __WEBPACK_IMPORTED_MODULE_13_ngx_perfect_scrollbar__["b" /* PerfectScrollbarModule */],
                __WEBPACK_IMPORTED_MODULE_14__angular_flex_layout__["a" /* FlexLayoutModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_material_toolbar__["a" /* MatToolbarModule */],
                __WEBPACK_IMPORTED_MODULE_10__angular_material_form_field__["c" /* MatFormFieldModule */],
                __WEBPACK_IMPORTED_MODULE_11__angular_material__["f" /* MatSidenavModule */],
                __WEBPACK_IMPORTED_MODULE_11__angular_material__["i" /* MatTabsModule */],
                __WEBPACK_IMPORTED_MODULE_11__angular_material__["g" /* MatSliderModule */],
                __WEBPACK_IMPORTED_MODULE_11__angular_material__["e" /* MatProgressBarModule */],
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__sidemenu_sidemenu_component__["a" /* SidemenuComponent */],
                __WEBPACK_IMPORTED_MODULE_3__sidemenu_item_sidemenu_item_component__["a" /* SidemenuItemComponent */],
                __WEBPACK_IMPORTED_MODULE_15__toolbar_notification_toolbar_notification_component__["a" /* ToolbarNotificationComponent */],
                __WEBPACK_IMPORTED_MODULE_16__toolbar_toolbar_component__["a" /* ToolbarComponent */],
                __WEBPACK_IMPORTED_MODULE_17__search_bar_search_bar_component__["a" /* SearchBarComponent */],
                __WEBPACK_IMPORTED_MODULE_18__fullscreen_fullscreen_component__["a" /* FullscreenComponent */],
                __WEBPACK_IMPORTED_MODULE_19__sidebar_sidebar_component__["a" /* SidebarComponent */],
                __WEBPACK_IMPORTED_MODULE_20__user_menu_user_menu_component__["a" /* UserMenuComponent */]
            ],
            providers: [
                {
                    provide: __WEBPACK_IMPORTED_MODULE_13_ngx_perfect_scrollbar__["a" /* PERFECT_SCROLLBAR_CONFIG */],
                    useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
                }
            ]
        })
    ], CoreModule);
    return CoreModule;
}());



/***/ }),

/***/ "../../../../../src/app/core/fullscreen/fullscreen.component.html":
/***/ (function(module, exports) {

module.exports = "<button mat-icon-button [fxHide]=\"true\" [fxHide.gt-xs]=\"false\" (click)=\"toggleFullscreen()\">\r\n    <mat-icon *ngIf=\"!isFullscreen\">fullscreen</mat-icon>\r\n    <mat-icon *ngIf=\"isFullscreen\">fullscreen_exit</mat-icon>\r\n</button>"

/***/ }),

/***/ "../../../../../src/app/core/fullscreen/fullscreen.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/core/fullscreen/fullscreen.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FullscreenComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_screenfull__ = __webpack_require__("../../../../screenfull/dist/screenfull.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_screenfull___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_screenfull__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FullscreenComponent = /** @class */ (function () {
    function FullscreenComponent() {
        this.isFullscreen = false;
    }
    FullscreenComponent.prototype.ngOnInit = function () {
    };
    FullscreenComponent.prototype.toggleFullscreen = function () {
        if (__WEBPACK_IMPORTED_MODULE_1_screenfull__["enabled"]) {
            __WEBPACK_IMPORTED_MODULE_1_screenfull__["toggle"]();
            this.isFullscreen = !this.isFullscreen;
        }
    };
    FullscreenComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'cdk-fullscreen',
            template: __webpack_require__("../../../../../src/app/core/fullscreen/fullscreen.component.html"),
            styles: [__webpack_require__("../../../../../src/app/core/fullscreen/fullscreen.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], FullscreenComponent);
    return FullscreenComponent;
}());



/***/ }),

/***/ "../../../../../src/app/core/search-bar/search-bar.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-form-field class=\"search\"  [ngClass]=\"{'search-open': open == true}\"> \r\n  <input matInput placeholder=\"Search\" autocomplete=\"off\" (focus)=\"bigMenu = true\" (focusout)=\"bigMenu = false\">\r\n</mat-form-field>"

/***/ }),

/***/ "../../../../../src/app/core/search-bar/search-bar.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".search {\n  width: 0;\n  overflow: hidden;\n  opacity: 0;\n  visibility: hidden;\n  -webkit-transition: all 0.4s cubic-bezier(0.35, 0, 0.25, 1);\n  transition: all 0.4s cubic-bezier(0.35, 0, 0.25, 1);\n  margin-top: 7px; }\n\n.search.search-open {\n  width: 250px;\n  visibility: visible;\n  opacity: 1;\n  margin-top: 11px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/core/search-bar/search-bar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchBarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SearchBarComponent = /** @class */ (function () {
    function SearchBarComponent() {
    }
    SearchBarComponent.prototype.ngOnInit = function () { };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
        __metadata("design:type", Object)
    ], SearchBarComponent.prototype, "open", void 0);
    SearchBarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'cdk-search-bar',
            template: __webpack_require__("../../../../../src/app/core/search-bar/search-bar.component.html"),
            styles: [__webpack_require__("../../../../../src/app/core/search-bar/search-bar.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], SearchBarComponent);
    return SearchBarComponent;
}());



/***/ }),

/***/ "../../../../../src/app/core/sidebar/sidebar.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-tab-group>\r\n    <mat-tab label=\"Overview\">\r\n            \r\n        <div class=\"sidebar\">\r\n        \r\n        <div fxLayout=\"column\" fxLayoutAlign=\"center center\" style=\"padding: 30px 0; background: url('./assets/images/bg/city1.jpg');\">\r\n            <div style=\"width: 100%; height: 22%; position: absolute;background: #17161642;z-index: 1\"></div>\r\n            <div class=\"mat-display-2\" style=\"margin: 0; color: white;z-index: 2\">{{today | date:'shortTime'}}</div>\r\n            <div class=\"mat-subheading-2\" style=\"margin: 0;color: white;z-index: 2\">{{today | date:'EEEE'}}, {{today | date:'MMMM'}} {{today | date:'dd'}}\r\n            </div>\r\n        </div>\r\n\r\n\r\n        <mat-divider></mat-divider>\r\n\r\n        <h3 class=\"mat-subheading-2\" style=\"margin: 10px 14px 0;\">Upcoming Events</h3>\r\n        <mat-nav-list>\r\n           <div *ngFor=\"let event of events;\">\r\n                <mat-list-item >\r\n                    <div fxLayout=\"row\" fxLayoutAlign=\"start center\" mat-ripple>\r\n                         <div fxLayout=\"column\">\r\n                            <div class=\"mat-ubheading-2\"  style=\"margin: 0; font-weight: bold;\">{{ event.title }}</div>\r\n                            <div style=\"font-size: 12px;\">{{ event.time }}</div>\r\n                        </div>\r\n                    </div>\r\n                </mat-list-item>\r\n            </div>\r\n        </mat-nav-list>\r\n                \r\n                   \r\n        \r\n        <mat-divider></mat-divider>\r\n        \r\n        <mat-nav-list>\r\n            <h3 class=\"mat-subheading-2\" style=\"margin: 10px 14px 0;\">Todo-List</h3>\r\n            <div *ngFor=\"let todolist of todolists;\">\r\n                <mat-list-item >\r\n                    <div fxLayout=\"row\" fxLayoutAlign=\"start center\" mat-ripple>\r\n                        <div class=\"title\" fxLayout=\"column\" >\r\n                            <div class=\"mat-ubheading-2\"  style=\"margin: 0; font-weight: bold;\">{{ todolist.title }}</div>\r\n                            <div style=\"font-size: 12px;\">{{ todolist.time }}</div>\r\n                        </div>\r\n                    </div>\r\n                </mat-list-item>\r\n            </div>\r\n        </mat-nav-list>\r\n        \r\n        <mat-divider></mat-divider>\r\n      \r\n\r\n    </div>\r\n</mat-tab>\r\n\r\n<mat-tab label=\"Notifications\">\r\n    <div class=\"sidebar\">\r\n        <div>\r\n            <h3 style=\"margin: 10px 14px 0;\">Friends</h3>\r\n            \r\n            <mat-list>\r\n              <mat-list-item *ngFor=\"let message of messages\">\r\n                <img matListAvatar src=\"assets/images/avatars/friend1.jpeg\" alt=\"...\">\r\n                <h3 matLine style=\"margin: 0; font-weight: bold;\"> {{message.from}} </h3>\r\n                <p matLine>\r\n                  <span> {{message.subject}} </span>\r\n                  <span> -- {{message.content}} </span>\r\n                </p>\r\n              </mat-list-item>\r\n            </mat-list>\r\n            \r\n        </div>\r\n    </div>\r\n\r\n    </mat-tab>\r\n</mat-tab-group>\r\n\r\n<mat-divider></mat-divider>\r\n\r\n<mat-nav-list>\r\n    <div>\r\n        <h3 class=\"mat-subheading-2\" style=\"margin: 10px 14px 0;\">Server Statistics</h3>\r\n        <div>\r\n            <mat-list-item >\r\n                <h5>CPU Load</h5>\r\n                <mat-progress-bar\r\n                    [color]=\"'accent'\"\r\n                    [mode]=\"'buffer'\"\r\n                    [value]=\"'70'\"\r\n                    [bufferValue]=\"100\">></mat-progress-bar>\r\n            </mat-list-item>\r\n        </div>\r\n            \r\n        \r\n        <div>\r\n            <mat-list-item >\r\n                <h5>RAM Usage</h5>\r\n                <mat-progress-bar\r\n                    [color]=\"'primary'\"\r\n                    [mode]=\"'buffer'\"\r\n                    [value]=\"'47'\"\r\n                    [bufferValue]=\"100\"></mat-progress-bar>\r\n            </mat-list-item>\r\n        </div>\r\n        \r\n        \r\n        <div>\r\n            <mat-list-item>\r\n                <h5>CPU Temp</h5>\r\n                <mat-progress-bar\r\n                    [color]=\"'warn'\"\r\n                    [mode]=\"'buffer'\"\r\n                    [value]=\"'43'\"\r\n                    [bufferValue]=\"100\"></mat-progress-bar>\r\n            </mat-list-item>\r\n        </div>\r\n        \r\n    </div>\r\n</mat-nav-list>"

/***/ }),

/***/ "../../../../../src/app/core/sidebar/sidebar.component.scss":
/***/ (function(module, exports, __webpack_require__) {

var escape = __webpack_require__("../../../../css-loader/lib/url/escape.js");
exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".content {\n  margin-top: 2px; }\n\n.example-margin {\n  margin: 0 10px; }\n\n.today {\n  width: 100%;\n  height: 22%;\n  position: absolute;\n  background: '#17161642';\n  z-index: 1; }\n\n.today-bg {\n  padding: 30px 0;\n  background: url(" + escape(__webpack_require__("../../../../../src/assets/images/bg/city1.jpg")) + "); }\n\n.today-time {\n  margin: 0;\n  color: white;\n  z-index: 2; }\n\n.today-date {\n  margin: 0;\n  color: white;\n  z-index: 2; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/core/sidebar/sidebar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SidebarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SidebarComponent = /** @class */ (function () {
    function SidebarComponent() {
        this.today = Date.now();
        // public bufferValue;
        this.events = [
            {
                id: 'id',
                title: 'Business Meeting',
                time: '05:00 PM',
                state: 'state'
            },
            {
                id: 'id',
                title: 'Ask for a Vacation',
                time: '05:00 PM',
                state: 'state'
            },
            {
                id: 'id',
                title: 'Dinner with Micheal',
                time: '05:00 PM',
                state: 'state'
            },
            {
                id: 'id',
                title: 'Deadline for Project ABC',
                time: '05:00 PM',
                state: 'state'
            },
        ];
        this.todolists = [
            {
                id: 'id',
                title: 'Get to know Angular more',
                time: 'Added:4 days ago',
            },
            {
                id: 'id',
                title: 'Configure new Router',
                time: 'Added:4 days ago',
            },
            {
                id: 'id',
                title: 'Invite Joy to play Carroms',
                time: 'Added:4 days ago',
            },
            {
                id: 'id',
                title: 'Check SRS of Project X',
                time: 'Added:4 days ago',
            },
        ];
        this.messages = [
            { from: 'Catherin', subject: 'Shopping', content: 'hi there??' },
            { from: 'Jack', subject: 'Function', content: 'yes' },
            { from: 'Karina', subject: 'Get together', content: 'nice' },
            { from: 'Micheal', subject: 'Trip', content: 'ya.. I will' },
            { from: 'Ashik', subject: 'Meeting', content: 'Time??' },
            { from: 'Joy', subject: 'Party', content: 'Lets enjoy' },
        ];
    }
    SidebarComponent.prototype.ngOnInit = function () {
    };
    SidebarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'cdk-sidebar',
            template: __webpack_require__("../../../../../src/app/core/sidebar/sidebar.component.html"),
            styles: [__webpack_require__("../../../../../src/app/core/sidebar/sidebar.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], SidebarComponent);
    return SidebarComponent;
}());



/***/ }),

/***/ "../../../../../src/app/core/sidemenu-item/sidemenu-item.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-nav-list  [style.height]=\"getHeight()\" [ngClass]=\"{'secondaryMenu': secondaryMenu}\">\r\n\r\n\r\n    <mat-list-item *ngIf = \"menu.link==false\" (click)=\"menu.open = !menu.open\" >\r\n    \t\r\n    \t<mat-icon matListIcon iconsmall  >{{menu.icon}} </mat-icon>\r\n\r\n        <h3 matLine *ngIf=\"!iconOnly\">{{ menu.name }} </h3>\r\n        <mat-chip-list *ngIf=\"menu?.chip && !iconOnly\">\r\n            <mat-chip >{{menu?.chip?.value}} </mat-chip>\r\n        </mat-chip-list>        \r\n        <mat-icon *ngIf=\"chechForChildMenu()\" class=\"sidenav-dropdown-indicator rotate \" [ngClass]=\"{'indicateOpen':menu.open}\"> expand_more</mat-icon>\r\n    \r\n    </mat-list-item>\r\n    <mat-list-item *ngIf = \"menu.link!=false\" (click)=\"menu.open = !menu.open\" [routerLink]=\"[menu.link]\">\r\n    \t<mat-icon matListIcon iconsmall  >{{menu.icon}} </mat-icon>\r\n        <h3 matLine *ngIf=\"!iconOnly\">{{ menu.name }} </h3>\r\n    </mat-list-item>\r\n    <cdk-sidemenu-item *ngFor=\"let submenu of menu?.sub\" [menu]=\"submenu\" [iconOnly]=\"iconOnly\" [secondaryMenu]=\"true\"> </cdk-sidemenu-item>\r\n</mat-nav-list>\r\n\r\n\r\n \r\n\r\n"

/***/ }),

/***/ "../../../../../src/app/core/sidemenu-item/sidemenu-item.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/core/sidemenu-item/sidemenu-item.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SidemenuItemComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SidemenuItemComponent = /** @class */ (function () {
    function SidemenuItemComponent() {
        this.secondaryMenu = false;
    }
    SidemenuItemComponent.prototype.ngOnInit = function () {
    };
    SidemenuItemComponent.prototype.openLink = function () {
        this.menu.open = this.menu.open;
    };
    SidemenuItemComponent.prototype.getHeight = function () {
        if (this.menu.open == false)
            return '48px';
        else {
            if (this.menu && this.menu.sub) {
                var height = (this.menu.sub.length * 56) + 56 + "px";
                return height;
            }
        }
    };
    SidemenuItemComponent.prototype.chechForChildMenu = function () {
        return (this.menu && this.menu.sub) ? true : false;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
        __metadata("design:type", Object)
    ], SidemenuItemComponent.prototype, "menu", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
        __metadata("design:type", Boolean)
    ], SidemenuItemComponent.prototype, "iconOnly", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
        __metadata("design:type", Boolean)
    ], SidemenuItemComponent.prototype, "secondaryMenu", void 0);
    SidemenuItemComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'cdk-sidemenu-item',
            template: __webpack_require__("../../../../../src/app/core/sidemenu-item/sidemenu-item.component.html"),
            styles: [__webpack_require__("../../../../../src/app/core/sidemenu-item/sidemenu-item.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], SidemenuItemComponent);
    return SidemenuItemComponent;
}());



/***/ }),

/***/ "../../../../../src/app/core/sidemenu/menu-element.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return menus; });
var menus = [
    {
        'name': 'Sample',
        'icon': 'dashboard',
        'link': 'sample',
        'open': false
    },
    {
        'name': 'Kek',
        'icon': 'dashboard',
        'link': false,
        'open': false
    }
];


/***/ }),

/***/ "../../../../../src/app/core/sidemenu/sidemenu.component.html":
/***/ (function(module, exports) {

module.exports = "\r\n<perfect-scrollbar style=\"height: calc(100% - 33px);\">\r\n    <div fxLayout=\"column\" >\r\n        <cdk-sidemenu-item *ngFor=\"let menu of menus\" [menu]=\"menu\" [iconOnly]=\"iconOnly\"> </cdk-sidemenu-item>\r\n    </div>\r\n    \r\n</perfect-scrollbar>\r\n"

/***/ }),

/***/ "../../../../../src/app/core/sidemenu/sidemenu.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/core/sidemenu/sidemenu.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SidemenuComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__menu_element__ = __webpack_require__("../../../../../src/app/core/sidemenu/menu-element.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SidemenuComponent = /** @class */ (function () {
    function SidemenuComponent() {
        this.iconOnly = false;
        this.menus = __WEBPACK_IMPORTED_MODULE_1__menu_element__["a" /* menus */];
    }
    SidemenuComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
        __metadata("design:type", Boolean)
    ], SidemenuComponent.prototype, "iconOnly", void 0);
    SidemenuComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'cdk-sidemenu',
            template: __webpack_require__("../../../../../src/app/core/sidemenu/sidemenu.component.html"),
            styles: [__webpack_require__("../../../../../src/app/core/sidemenu/sidemenu.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], SidemenuComponent);
    return SidemenuComponent;
}());



/***/ }),

/***/ "../../../../../src/app/core/toolbar-notification/toolbar-notification.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"toolbar-notification-container\">\r\n\t<button mat-icon-button (click)=\"isOpen = !isOpen;\" [ngClass]=\"[cssPrefix+'-btn']\" [class.open]=\"isOpen\">\r\n    \t<mat-icon>notifications_none</mat-icon>\r\n    \t<span class=\"badge\" *ngIf=\"notifications && notifications?.length !== 0\">{{ notifications?.length }}</span>\r\n  </button>\r\n\r\n\r\n\t<div class=\"dropdown mat-elevation-z4\" [class.open]=\"isOpen\">\r\n\r\n\t<div class=\"card\">\r\n      \t<div class=\"header\" fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\r\n        \t<div class=\"title\">\r\n         \t\t <div class=\"name\">Notifications</div>\r\n          \t\t<div class=\"extra\">\r\nYou have {{ notifications?.length }} new notifications</div>\r\n        \t</div>\r\n        \t<button type=\"button\" mat-icon-button>\r\n          \t\t<mat-icon class=\"icon\">settings</mat-icon>\r\n        \t</button>\r\n      \t</div>\r\n      \t<div *ngIf=\"notifications?.length !== 0; then thenBlock else elseBlock;\"></div>\r\n      \t<div class=\"footer\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n        \t<div class=\"action\">Mark all as read</div>\r\n    \t</div>\r\n    </div>\r\n\r\n\t</div>\r\n\r\n</div>\r\n\r\n<ng-template #thenBlock>\r\n  <perfect-scrollbar class=\"content\">\r\n    <div *ngFor=\"let notification of notifications; last as isLast\">\r\n      <div class=\"notification\" fxLayout=\"row\" fxLayoutAlign=\"start center\" mat-ripple>\r\n        <mat-icon class=\"icon\">notifications</mat-icon>\r\n        <div class=\"title\" fxLayout=\"column\">\r\n          <div class=\"name\">{{ notification.title }}</div>\r\n          <div class=\"time\">{{ notification.lastTime }}</div>\r\n        </div>\r\n        <span fxFlex></span>\r\n        <button type=\"button\" mat-icon-button (click)=\"delete(notification)\">\r\n          <mat-icon class=\"close\">close</mat-icon>\r\n        </button>\r\n      </div>\r\n      <div class=\"divider\" *ngIf=\"!isLast\"></div>\r\n    </div>\r\n  </perfect-scrollbar>\r\n</ng-template>\r\n\r\n<ng-template #elseBlock>\r\n  <div class=\"no\" fxLayout=\"row\" fxLayoutAlign=\"center center\">暂无通知</div>\r\n</ng-template>\r\n\r\n"

/***/ }),

/***/ "../../../../../src/app/core/toolbar-notification/toolbar-notification.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".badge {\n  position: absolute;\n  top: 0;\n  left: 50%;\n  font-weight: 700;\n  line-height: 13px;\n  height: 13px;\n  padding: 5px;\n  border-radius: 26%;\n  width: 30%;\n  background-color: #f44336;\n  color: #fff;\n  border-color: #f44336; }\n\n.toolbar-notification-container {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n\n.toolbar-notification-btn {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  margin-right: 10px; }\n\n.dropdown {\n  background: white;\n  position: absolute;\n  top: 42px;\n  right: 28px;\n  min-width: 350px;\n  z-index: 2;\n  -webkit-transform: translateY(0) scale(0);\n          transform: translateY(0) scale(0);\n  -webkit-transform-origin: top right;\n          transform-origin: top right;\n  visibility: hidden;\n  -webkit-transition: visibility 0.4s cubic-bezier(0.25, 0.8, 0.25, 1), -webkit-transform 0.4s cubic-bezier(0.25, 0.8, 0.25, 1);\n  transition: visibility 0.4s cubic-bezier(0.25, 0.8, 0.25, 1), -webkit-transform 0.4s cubic-bezier(0.25, 0.8, 0.25, 1);\n  transition: transform 0.4s cubic-bezier(0.25, 0.8, 0.25, 1), visibility 0.4s cubic-bezier(0.25, 0.8, 0.25, 1);\n  transition: transform 0.4s cubic-bezier(0.25, 0.8, 0.25, 1), visibility 0.4s cubic-bezier(0.25, 0.8, 0.25, 1), -webkit-transform 0.4s cubic-bezier(0.25, 0.8, 0.25, 1); }\n\n@media screen and (max-width: 599px) {\n    .dropdown {\n      min-width: 50vw;\n      right: 5px;\n      -webkit-transform: translateY(0);\n              transform: translateY(0);\n      visibility: hidden;\n      -webkit-transition: visibility 0.4s cubic-bezier(0.25, 0.8, 0.25, 1), -webkit-transform 0.4s cubic-bezier(0.25, 0.8, 0.25, 1);\n      transition: visibility 0.4s cubic-bezier(0.25, 0.8, 0.25, 1), -webkit-transform 0.4s cubic-bezier(0.25, 0.8, 0.25, 1);\n      transition: transform 0.4s cubic-bezier(0.25, 0.8, 0.25, 1), visibility 0.4s cubic-bezier(0.25, 0.8, 0.25, 1);\n      transition: transform 0.4s cubic-bezier(0.25, 0.8, 0.25, 1), visibility 0.4s cubic-bezier(0.25, 0.8, 0.25, 1), -webkit-transform 0.4s cubic-bezier(0.25, 0.8, 0.25, 1); } }\n\n.dropdown.open {\n    -webkit-transform: translateY(0) scale(1);\n            transform: translateY(0) scale(1);\n    visibility: visible; }\n\n.dropdown .card .header {\n    background: #EEEEEE;\n    min-height: 54px;\n    padding-left: 16px;\n    padding-right: 8px;\n    color: #555;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: start;\n        -ms-flex-pack: start;\n            justify-content: flex-start;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -ms-flex-line-pack: center;\n        align-content: center;\n    border-bottom: 1px solid #e0e0e0; }\n\n.dropdown .card .header .extra {\n      font-size: 12px;\n      color: #888; }\n\n.dropdown .content {\n    overflow: hidden;\n    max-height: 256px; }\n\n.dropdown .content .notification {\n      min-height: 64px;\n      padding: 0 16px 0 14px;\n      position: relative;\n      color: #666;\n      cursor: pointer; }\n\n.dropdown .content .notification .icon {\n        height: 28px;\n        width: 28px;\n        line-height: 28px;\n        font-size: 18px;\n        margin-right: 13px;\n        text-align: center;\n        border-radius: 50%;\n        background: #FFF;\n        color: #888;\n        border: 1px solid #EEE; }\n\n.dropdown .content .notification .title {\n        font-weight: 500;\n        font-size: 14px; }\n\n.dropdown .content .notification .time {\n        font-size: 12px; }\n\n.dropdown .content .notification .close {\n        font-size: 18px;\n        width: 18px;\n        height: 18px;\n        line-height: 18px; }\n\n.dropdown .content .notification.primary .icon {\n        background: #ccc;\n        color: #ddd; }\n\n.dropdown .content .notification.accent .icon {\n        background: #aaa;\n        color: #bbb; }\n\n.dropdown .content .notification.warn .icon {\n        background: #eee;\n        color: #ddd; }\n\n.dropdown .content .notification.read {\n        color: #999; }\n\n.dropdown .content .notification.read .name {\n          font-weight: normal; }\n\n.dropdown .footer {\n    min-height: 42px;\n    border-top: 1px solid #EEE; }\n\n.dropdown .footer .action {\n      cursor: pointer;\n      color: #AAA;\n      text-align: center;\n      font-size: 13px; }\n\n.dropdown .divider {\n    width: calc(100% - 30px);\n    height: 1px;\n    background: #EEE;\n    margin: 0 16px 0 14px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/core/toolbar-notification/toolbar-notification.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ToolbarNotificationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ToolbarNotificationComponent = /** @class */ (function () {
    // @HostListener('document:click', ['$event', '$event.target'])
    // onClick(event: MouseEvent, targetElement: HTMLElement) {
    //     if (!targetElement) {
    //           return;
    //     }
    //     const clickedInside = this.elementRef.nativeElement.contains(targetElement);
    //     if (!clickedInside) {
    //          this.isOpen = false;
    //     }
    // }
    function ToolbarNotificationComponent(elementRef) {
        this.elementRef = elementRef;
        this.cssPrefix = 'toolbar-notification';
        this.isOpen = false;
        this.notifications = [];
    }
    ToolbarNotificationComponent.prototype.ngOnInit = function () {
    };
    ToolbarNotificationComponent.prototype.select = function () {
    };
    ToolbarNotificationComponent.prototype.delete = function (notification) {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
        __metadata("design:type", Object)
    ], ToolbarNotificationComponent.prototype, "notifications", void 0);
    ToolbarNotificationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'cdk-toolbar-notification',
            template: __webpack_require__("../../../../../src/app/core/toolbar-notification/toolbar-notification.component.html"),
            styles: [__webpack_require__("../../../../../src/app/core/toolbar-notification/toolbar-notification.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */]])
    ], ToolbarNotificationComponent);
    return ToolbarNotificationComponent;
}());



/***/ }),

/***/ "../../../../../src/app/core/toolbar/toolbar.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- <stbui-loading *ngIf=\"showLoading\"></stbui-loading> -->\r\n<mat-toolbar  class=\"mat-elevation-z4\">\r\n\t<button mat-icon-button (click)=\"sidenav.toggle();drawer.toggle();\" *ngIf=\"matDrawerShow\">\r\n        <i class=\"material-icons app-toolbar-menu\">menu </i>\r\n    </button> \r\n    <button mat-icon-button (click)=\"sidenav.toggle();\" *ngIf=\"!matDrawerShow\">\r\n        <i class=\"material-icons app-toolbar-menu\">menu </i>\r\n    </button>\r\n\r\n    <span class=\"spacer\"></span>\r\n\r\n\r\n    <cdk-fullscreen></cdk-fullscreen>\r\n\r\n\r\n    \r\n</mat-toolbar>"

/***/ }),

/***/ "../../../../../src/app/core/toolbar/toolbar.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  z-index: 4; }\n\n.main-toolbar {\n  height: 64px;\n  padding-left: 16px; }\n\n.more-btn {\n  height: 100%;\n  min-width: 70px; }\n\n.mat-icon-button {\n  margin-right: 10px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/core/toolbar/toolbar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ToolbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__toolbar_helpers__ = __webpack_require__("../../../../../src/app/core/toolbar/toolbar.helpers.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ToolbarComponent = /** @class */ (function () {
    function ToolbarComponent() {
        this.searchOpen = false;
        this.toolbarHelpers = __WEBPACK_IMPORTED_MODULE_1__toolbar_helpers__["a" /* ToolbarHelpers */];
    }
    ToolbarComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
        __metadata("design:type", Object)
    ], ToolbarComponent.prototype, "sidenav", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
        __metadata("design:type", Object)
    ], ToolbarComponent.prototype, "sidebar", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
        __metadata("design:type", Object)
    ], ToolbarComponent.prototype, "drawer", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
        __metadata("design:type", Object)
    ], ToolbarComponent.prototype, "matDrawerShow", void 0);
    ToolbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'cdk-toolbar',
            template: __webpack_require__("../../../../../src/app/core/toolbar/toolbar.component.html"),
            styles: [__webpack_require__("../../../../../src/app/core/toolbar/toolbar.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ToolbarComponent);
    return ToolbarComponent;
}());



/***/ }),

/***/ "../../../../../src/app/core/toolbar/toolbar.helpers.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ToolbarHelpers; });
var ToolbarHelpers = {
    notifications: [
        {
            id: 'id',
            title: 'Mail 5',
            lastTime: '23 Minutes ago',
            state: 'state'
        },
        {
            id: 'id',
            title: 'Mail 5',
            lastTime: '23 Minutes ago',
            state: 'state'
        },
        {
            id: 'id',
            title: 'Mail 5',
            lastTime: '23 Minutes ago',
            state: 'state'
        },
    ],
    currentUser: {
        photoURL: 'assets/images/avatars/hari.jpg',
        currentUserName: 'Hari Krishna'
    }
};


/***/ }),

/***/ "../../../../../src/app/core/user-menu/user-menu.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"toolbar-user-container\">\r\n\t<button mat-button (click)=\"isOpen = !isOpen\" class=\"toolbar-user-btn\" [class.open]=\"isOpen\" [ngStyle.xs]=\"{'min-width': '70px'}\">\r\n  \t\t<span fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n    \t\t<img class=\"avatar\" [src]=\"currentUser?.photoURL || 'assets/images/avatars/noavatar.png'\">\r\n    \t\t<span class=\"name\" fxHide fxShow.gt-xs>{{currentUser?.currentUserName || Hari}}</span>\r\n    \t\t<mat-icon class=\"icon\" fxHide fxShow.gt-xs>keyboard_arrow_down</mat-icon>\r\n  \t\t</span>\r\n  \t</button>\r\n\r\n\r\n  \t<div class=\"dropdown mat-elevation-z1\" [class.open]=\"isOpen\">\r\n    \t<div class=\"content\">\r\n      \t\t<mat-nav-list>\r\n      \t\t\t<mat-list-item>\r\n      \t\t\t\t<a matLine >Profile</a>\r\n\t\t\t\t    <button mat-icon-button>\r\n\t\t\t\t       <mat-icon>account_circle</mat-icon>\r\n\t\t\t\t    </button>\r\n      \t\t\t</mat-list-item>\r\n      \t\t\t<mat-list-item>\r\n      \t\t\t\t<a matLine >Settings</a>\r\n\t\t\t\t    <button mat-icon-button>\r\n\t\t\t\t       <mat-icon>settings</mat-icon>\r\n\t\t\t\t    </button>\r\n      \t\t\t</mat-list-item>\r\n      \t\t\t<mat-list-item>\r\n      \t\t\t\t<a matLine >Help</a>\r\n\t\t\t\t    <button mat-icon-button>\r\n\t\t\t\t       <mat-icon>help</mat-icon>\r\n\t\t\t\t    </button>\r\n      \t\t\t</mat-list-item>\r\n\r\n      \t\t\t<mat-divider></mat-divider>\r\n\r\n      \t\t\t<mat-list-item>\r\n      \t\t\t\t<a matLine >Logout</a>\r\n\t\t\t\t    <button mat-icon-button>\r\n\t\t\t\t       <mat-icon>exit_to_app</mat-icon>\r\n\t\t\t\t    </button>\r\n      \t\t\t</mat-list-item>\r\n      \t\t</mat-nav-list>\r\n    \t</div>\r\n  \t</div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/core/user-menu/user-menu.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  height: 100%; }\n\n.toolbar-user-container {\n  height: 100%;\n  position: relative; }\n\n.toolbar-user-container .toolbar-user-btn {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    height: 100%;\n    min-width: 160px; }\n\n.toolbar-user-container .toolbar-user-btn .avatar {\n      width: 30px;\n      height: 30px;\n      border-radius: 50%; }\n\n.toolbar-user-container .toolbar-user-btn .name {\n      margin: 0 8px 0 10px; }\n\n.toolbar-user-container .toolbar-user-btn .icon {\n      width: 16px;\n      height: 16px;\n      font-size: 16px;\n      -webkit-transform: rotate(0);\n              transform: rotate(0);\n      -webkit-transition: -webkit-transform 0.25s cubic-bezier(0.25, 0.8, 0.25, 1);\n      transition: -webkit-transform 0.25s cubic-bezier(0.25, 0.8, 0.25, 1);\n      transition: transform 0.25s cubic-bezier(0.25, 0.8, 0.25, 1);\n      transition: transform 0.25s cubic-bezier(0.25, 0.8, 0.25, 1), -webkit-transform 0.25s cubic-bezier(0.25, 0.8, 0.25, 1); }\n\n.toolbar-user-container .toolbar-user-btn.open {\n      background: rgba(0, 0, 0, 0.05); }\n\n.toolbar-user-container .toolbar-user-btn.open .icon {\n        -webkit-transform: rotate(-180deg);\n                transform: rotate(-180deg); }\n\n.toolbar-user-container .dropdown {\n    background: white;\n    z-index: 2;\n    position: absolute;\n    width: 100%;\n    min-width: 160px;\n    opacity: 0;\n    visibility: hidden;\n    -webkit-transition: all .25s linear, max-height .25s linear, opacity .25s linear;\n    transition: all .25s linear, max-height .25s linear, opacity .25s linear; }\n\n@media screen and (max-width: 599px) {\n      .toolbar-user-container .dropdown {\n        min-width: 65px; } }\n\n.toolbar-user-container .dropdown.open {\n      opacity: 1;\n      visibility: visible; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/core/user-menu/user-menu.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserMenuComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var UserMenuComponent = /** @class */ (function () {
    function UserMenuComponent(elementRef) {
        this.elementRef = elementRef;
        this.isOpen = false;
        this.currentUser = null;
    }
    UserMenuComponent.prototype.onClick = function (event, targetElement) {
        if (!targetElement) {
            return;
        }
        var clickedInside = this.elementRef.nativeElement.contains(targetElement);
        if (!clickedInside) {
            this.isOpen = false;
        }
    };
    UserMenuComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
        __metadata("design:type", Object)
    ], UserMenuComponent.prototype, "currentUser", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* HostListener */])('document:click', ['$event', '$event.target']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [MouseEvent, HTMLElement]),
        __metadata("design:returntype", void 0)
    ], UserMenuComponent.prototype, "onClick", null);
    UserMenuComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'cdk-user-menu',
            template: __webpack_require__("../../../../../src/app/core/user-menu/user-menu.component.html"),
            styles: [__webpack_require__("../../../../../src/app/core/user-menu/user-menu.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */]])
    ], UserMenuComponent);
    return UserMenuComponent;
}());



/***/ }),

/***/ "../../../../../src/app/lazy-load/lazy-load.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LazyLoadModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var routes = [
    { path: 'auth', loadChildren: '../auth/auth.module#AuthModule' },
    { path: 'register', loadChildren: '../register/register.module#RegisterModule' },
    // {path: 'login', loadChildren: '../pages/login/login.module#LoginModule'},
    // {path: 'editor', loadChildren: '../editor/editor.module#EditorModule'},
    { path: '**', redirectTo: 'auth/forms' },
];
var LazyLoadModule = /** @class */ (function () {
    function LazyLoadModule() {
    }
    LazyLoadModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* NgModule */])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* RouterModule */].forRoot(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* RouterModule */]]
        })
    ], LazyLoadModule);
    return LazyLoadModule;
}());



/***/ }),

/***/ "../../../../../src/assets/images/bg/city1.jpg":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "city1.5ccf7022f41053fd8b67.jpg";

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
/* unused harmony export palete */
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false,
    firebase: {
        apiKey: 'AIzaSyBwlk0KMM75nBsZ8L7j9cZ5N5x0fwqfBjg',
        authDomain: 'newproject-5d731.firebaseapp.com',
        databaseURL: 'https://newproject-5d731.firebaseio.com',
        projectId: 'newproject-5d731',
        storageBucket: 'newproject-5d731.appspot.com',
        messagingSenderId: '737689833575'
    },
    mailApi: '/assets/list.json'
};
var palete = {
    primary: '#D32F2F',
    accent: '#E65100',
    warm: '#C2185B',
    name: '#D50000',
    secondary: '#D81B60',
    tertiary: '#8E24AA',
    quaternary: '#5E35B1',
    quinary: '#3949AB',
    secondaryLight: '#E91E63',
    tertiaryLight: '#9C27B0',
    quaternaryLight: '#673AB7',
    quinaryLight: '#3F51B5'
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_hammerjs__ = __webpack_require__("../../../../hammerjs/hammer.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_hammerjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_hammerjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");





if (__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_18" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_2__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map