webpackJsonp(["dashboard-accounts.module"],{

/***/ "../../../../../src/app/dashboard-accounts/dashboard-accounts.component.html":
/***/ (function(module, exports) {

module.exports = "<div fxLayout=\"row\" fxLayoutWrap>\r\n  <div  fxLayoutGap=\"10px\" fxFlex.lt-sm=\"100\"  fxFlex.sm=\"50\" fxFlex.md=\"33.3\"  *ngFor=\"let navigation of [1,2,3,4,5]\">\r\n    <mat-card>\r\n      <mat-card-title class=\"nav-title\">\r\n           11\r\n        </mat-card-title>\r\n    </mat-card>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/dashboard-accounts/dashboard-accounts.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "mat-card {\n  padding: 10px;\n  margin: 10px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/dashboard-accounts/dashboard-accounts.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardAccountsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DashboardAccountsComponent = /** @class */ (function () {
    function DashboardAccountsComponent() {
    }
    DashboardAccountsComponent.prototype.ngOnInit = function () {
    };
    DashboardAccountsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-dashboard-accounts',
            template: __webpack_require__("../../../../../src/app/dashboard-accounts/dashboard-accounts.component.html"),
            styles: [__webpack_require__("../../../../../src/app/dashboard-accounts/dashboard-accounts.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], DashboardAccountsComponent);
    return DashboardAccountsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/dashboard-accounts/dashboard-accounts.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "appRoutes", function() { return appRoutes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardAccountsModule", function() { return DashboardAccountsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dashboard_accounts_component__ = __webpack_require__("../../../../../src/app/dashboard-accounts/dashboard-accounts.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_flex_layout__ = __webpack_require__("../../../flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material_card__ = __webpack_require__("../../../material/esm5/card.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var appRoutes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_2__dashboard_accounts_component__["a" /* DashboardAccountsComponent */] },
];
var DashboardAccountsModule = /** @class */ (function () {
    function DashboardAccountsModule() {
    }
    DashboardAccountsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_flex_layout__["a" /* FlexLayoutModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_material_card__["a" /* MatCardModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* RouterModule */].forChild(appRoutes),
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__dashboard_accounts_component__["a" /* DashboardAccountsComponent */]]
        })
    ], DashboardAccountsModule);
    return DashboardAccountsModule;
}());



/***/ })

});
//# sourceMappingURL=dashboard-accounts.module.chunk.js.map