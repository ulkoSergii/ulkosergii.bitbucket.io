webpackJsonp(["forms.module"],{

/***/ "../../../../../src/app/forms/forms.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormModule", function() { return FormModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__forms_router__ = __webpack_require__("../../../../../src/app/forms/forms.router.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__reactive_forms_reactive_forms_component__ = __webpack_require__("../../../../../src/app/forms/reactive-forms/reactive-forms.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__template_driven_forms_template_driven_forms_component__ = __webpack_require__("../../../../../src/app/forms/template-driven-forms/template-driven-forms.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_flex_layout__ = __webpack_require__("../../../flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var FormModule = /** @class */ (function () {
    function FormModule() {
    }
    FormModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_2__forms_router__["a" /* FormsRouterModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_flex_layout__["a" /* FlexLayoutModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["a" /* MatButtonModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["j" /* MatToolbarModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["b" /* MatCardModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["i" /* MatTabsModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["c" /* MatIconModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["d" /* MatInputModule */],
                __WEBPACK_IMPORTED_MODULE_7__angular_forms__["i" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_7__angular_forms__["d" /* FormsModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_3__reactive_forms_reactive_forms_component__["a" /* ReactiveFormsComponent */], __WEBPACK_IMPORTED_MODULE_4__template_driven_forms_template_driven_forms_component__["a" /* TemplateDrivenFormsComponent */]]
        })
    ], FormModule);
    return FormModule;
}());



/***/ }),

/***/ "../../../../../src/app/forms/forms.router.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormsRouterModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__reactive_forms_reactive_forms_component__ = __webpack_require__("../../../../../src/app/forms/reactive-forms/reactive-forms.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__template_driven_forms_template_driven_forms_component__ = __webpack_require__("../../../../../src/app/forms/template-driven-forms/template-driven-forms.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var FormsRoutes = [
    { path: 'reactive_forms', component: __WEBPACK_IMPORTED_MODULE_2__reactive_forms_reactive_forms_component__["a" /* ReactiveFormsComponent */] },
    { path: 'template_forms', component: __WEBPACK_IMPORTED_MODULE_3__template_driven_forms_template_driven_forms_component__["a" /* TemplateDrivenFormsComponent */] },
];
var FormsRouterModule = /** @class */ (function () {
    function FormsRouterModule() {
    }
    FormsRouterModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* RouterModule */].forChild(FormsRoutes)
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* RouterModule */]
            ]
        })
    ], FormsRouterModule);
    return FormsRouterModule;
}());



/***/ }),

/***/ "../../../../../src/app/forms/reactive-forms/reactive-forms.component.html":
/***/ (function(module, exports) {

module.exports = "<div fxLayout=\"column\"  class=\"components-container-gt-xs\"  [ngClass.xs]=\"'components-container-xs'\"  fxLayoutGap=\"20px\">\r\n\r\n\t<div fxFlex class=\"form-component-holder mat-elevation-z4\">\r\n\t\t<mat-toolbar class=\"form-header\">\r\n\t\t\t<h1 class=\"mat-headline center-align\" >Reactive Forms</h1>\r\n\t\t</mat-toolbar>\r\n\t\t    <div [hidden]=\"submitted\" fxLayout=\"column\"  class=\"widgetHolder-content\" fxLayoutAlign=\"space-around stretch\" style=\"background-color: white;\">\r\n       \r\n\t\t\r\n        <form [formGroup]=\"profileForm\"  (ngSubmit)=\"onSubmit()\" fxLayout=\"column\" fxFlex=\"100\" >\r\n\t\t\t<mat-form-field class=\"width\">\r\n\t\t\t\t<input type=\"text\" matInput placeholder=\"Enter Username\"  formControlName=\"username\" required>\r\n\t\t\t\r\n\t\t\t</mat-form-field>\r\n\t\t\t<mat-error *ngIf=\"username.invalid&& (username.dirty || username.touched)\" align=\"end\" style=\"color: red;\" class=\"mat-body-1 center-align \"> \r\n\t\t\t\t\r\n\t\t\t\t\tName must be at least 6 characters long.\r\n\t\t\t\t\r\n\t\t\t</mat-error>\r\n\r\n\t\t\t\r\n\t\t\t<mat-form-field  class=\"width\">\r\n\t\t\t\t<input type=\"email\" matInput placeholder=\"Enter Your Email\"  formControlName=\"email\"  required\r\n                pattern=\"^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$\"  required >\r\n\t\t\t</mat-form-field>\r\n\t\t\t<mat-error *ngIf=\"email.invalid && (email.dirty || email.touched)\" align=\"end\" style=\"color: red;\" class=\"mat-body-1 center-align\" >\r\n\t\t\t\t\r\n\t\t\t\t\tPlease enter valid email \r\n\t\t\t</mat-error>\r\n\t\t\t<mat-form-field  class=\"width\">\r\n\t\t\t\t<input type=\"text\" matInput placeholder=\"Enter your Number\"  \r\n\t\t\t\tformControlName=\"number\" required pattern=\"[0-9]*\">\r\n\t\t\t</mat-form-field>\r\n\t\t\t<mat-error *ngIf=\"number.hasError('minlength') && (number.dirty || number.touched)\" align=\"end\" style=\"color: red;\" class=\"mat-body-1 center-align\">\r\n\t\t\t\t\r\n\t\t\t\t\tPlease enter valid number \r\n\t\t\t</mat-error>\r\n\r\n\t\t\t<mat-form-field>\r\n    \t\t\t   <input matInput placeholder=\"Enter your password\" [type]=\"hide ? 'password' : 'text'\" formControlName=\"pwd\" >\r\n    \t\t\t   <mat-icon matSuffix (click)=\"hide = !hide\">{{hide ? 'visibility' : 'visibility_off'}}</mat-icon>\r\n    \t\t</mat-form-field>\r\n\t\t\t<button style=\"font-weight: bold;\" mat-raised-button type=\"submit\" *ngIf=\"username.valid && email.valid && number.valid\" class=\"mat-headline center-align\">Submit</button>\r\n\t\t</form>\r\n    </div>\r\n    <div [hidden]=\"!submitted\" style=\"background-color: white;\">\r\n\t\t<div fxLayout=\"column\" fxLayoutAlign=\"space-around stretch\" >\r\n\t\t\t<h2 class=\"mat-headline center-align\">You submitted the following:</h2>\r\n\t\t<div fxLayout=\"row\"  fxLayoutGap=\"20px\" class=\"mat-body center-align\">\r\n\t\t\t<div >Name</div>\r\n\t\t\t<div>{{ profileForm.value.username}}</div>\r\n\t\t</div>\r\n\t\t<div fxLayout=\"row\" fxLayoutGap=\"20px\" class=\"mat-body center-align\">\r\n\t\t\t<div>Number</div>\r\n\t\t\t<div>{{profileForm.value.number }}</div>\r\n\t\t</div>\r\n\t\t<div fxLayout=\"row\" fxLayoutGap=\"20px\"  class=\"mat-body center-align\">\r\n\t\t\t<div >Password</div>\r\n\t\t\t<div>{{ profileForm.value.pwd}}</div>\r\n\t\t</div>\r\n\t\t<div fxLayout=\"row\" fxLayoutGap=\"20px\" class=\"mat-body center-align\">\r\n\t\t\t<div >Email</div>\r\n\t\t\t<div>{{ profileForm.value.email}}</div>\r\n\t\t</div>\r\n\t\t<br>\r\n\t\t<button mat-raised-button type=\"submit\" (click)=\"submitted=false\">Edit</button>\r\n\t</div>\r\n\t</div>\r\n\t</div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/forms/reactive-forms/reactive-forms.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".form-header {\n  background-color: #327ebd;\n  color: white;\n  padding: 0 5px; }\n\n.mat-form-field {\n  padding-left: 10px; }\n\n.validation-message {\n  background-color: #00BCD4; }\n\n.container {\n  padding-left: 10px;\n  padding-right: 10px; }\n\n.form-container {\n  padding: 5px 15px 15px 15px; }\n\n.form-component-holder {\n  border: 1px solid #327ebd;\n  background-color: white; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/forms/reactive-forms/reactive-forms.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReactiveFormsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ReactiveFormsComponent = /** @class */ (function () {
    function ReactiveFormsComponent(form) {
        this.form = form;
        this.submitted = false;
        this.profileForm = this.form.group({
            username: ['', { validators: [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["j" /* Validators */].minLength(6)], updateOn: 'blur' }],
            email: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["j" /* Validators */].required],
            number: ['', { validators: [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["j" /* Validators */].minLength(10)], updateOn: 'blur' }],
            pwd: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["j" /* Validators */].required]
        });
    }
    Object.defineProperty(ReactiveFormsComponent.prototype, "number", {
        get: function () {
            return this.profileForm.get('number');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ReactiveFormsComponent.prototype, "username", {
        get: function () {
            return this.profileForm.get('username');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ReactiveFormsComponent.prototype, "email", {
        get: function () {
            return this.profileForm.get('email');
        },
        enumerable: true,
        configurable: true
    });
    // checkUserExists() {
    //         this.profileForm.value.userName.setErrors({ userExists: `User Name  already exists` });
    // }
    ReactiveFormsComponent.prototype.onSubmit = function () {
        console.log('');
        this.submitted = true;
    };
    ReactiveFormsComponent.prototype.ngOnInit = function () {
    };
    ReactiveFormsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'cdk-reactive-forms',
            template: __webpack_require__("../../../../../src/app/forms/reactive-forms/reactive-forms.component.html"),
            styles: [__webpack_require__("../../../../../src/app/forms/reactive-forms/reactive-forms.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormBuilder */]])
    ], ReactiveFormsComponent);
    return ReactiveFormsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/forms/template-driven-forms/template-driven-forms.component.html":
/***/ (function(module, exports) {

module.exports = "\r\n<div fxLayout=\"column\"  class=\"components-container-gt-xs\"  [ngClass.xs]=\"'components-container-xs'\"  fxLayoutGap=\"20px\">\r\n\r\n\t<div fxFlex class=\"form-component-holder mat-elevation-z4\">\r\n\t\t<mat-toolbar class=\"form-header\">\r\n\t\t\t<h1 class=\"mat-headline center-align\">Template Driven Forms</h1>\r\n\t\t</mat-toolbar>\r\n\t\t<div [hidden]=\"submitted\" style=\"background-color: white;\">\r\n\r\n\t\t\t<form (ngSubmit)=\"onSubmit()\" fxLayout=\"column\" >\r\n\t\t\t\t<mat-form-field >\r\n\t\t\t\t\t<input matInput type=\"text\" [(ngModel)]=\"model.name\" name=\"name\" #name=\"ngModel\" required minlength=\"4\">\r\n\t\t\t\t</mat-form-field>\r\n\r\n\t\t\t\t<div  style=\"color:red\" *ngIf=\"name.invalid && (name.dirty || name.touched)\" class=\"mat-body-1 center-align\">\r\n\r\n\t\t\t\t\t<div *ngIf=\"name.errors.required\">\r\n\t\t\t\t\t\tName is required.\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div *ngIf=\"name.errors.minlength\">\r\n\t\t\t\t\t\tName must be at least 4 characters long.\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<mat-form-field>\r\n\t\t\t\t\t<input matInput type=\"number\"  [(ngModel)]=\"model.number\"  name=\"number\" #number=\"ngModel\" required minlength=\"10\">\r\n\t\t\t\t</mat-form-field>\r\n\t\t\t\t<div  style=\"color:red\" *ngIf=\"number.invalid && (number.dirty || number.touched)\"\r\n\t\t\t\t>\r\n\t\t\t\t\t<div *ngIf=\"number.errors.required\">\r\n\t\t\t\t\t\tnumber is required.\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div *ngIf=\"number.errors.minlength\">\r\n\t\t\t\t\t\tnumber must be at least 10 characters long.\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<mat-input-container fxFlex>\r\n      \t\t\t\t<input matInput placeholder=\"Character count (max. 100)\" maxlength=\"100\"\t#characterCountHintExample value=\"Hello! How are you today?\">\r\n      \t\t\t\t<mat-hint align=\"end\">{{ characterCountHintExample.value.length }} / 100\r\n      \t\t\t\t</mat-hint>\r\n    \t\t\t</mat-input-container>\r\n    \t\t\t<mat-form-field>\r\n    \t\t\t   <input matInput placeholder=\"Enter your password\" [type]=\"hide ? 'password' : 'text'\" >\r\n    \t\t\t   <mat-icon matSuffix (click)=\"hide = !hide\">{{hide ? 'visibility' : 'visibility_off'}}</mat-icon>\r\n    \t\t\t </mat-form-field>\r\n\t\t\t\t<button mat-raised-button type=\"submit\"  [disabled]=\"name.invalid||number.invalid\" class=\"mat-subheading-2 center-align button-style\">Submit</button>\r\n\t\t\t</form>\r\n\t\t\t\r\n\t\t</div>\r\n\r\n\t\t<div [hidden]=\"!submitted\" style=\"background-color: white;\">\r\n\t\t\t<div fxLayout=\"column\" fxLayoutAlign=\"space-around stretch\" >\r\n\t\t\t\t<h2 class=\"mat-subheading-2 center-align\">You submitted the following:</h2>\r\n\t\t\t\t<div fxLayout=\"row\" fxLayoutGap=\"20px\" class=\"mat-body center-align\">\r\n\t\t\t\t\t<div >Name</div>\r\n\t\t\t\t\t<div>{{ model.name }}</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div fxLayout=\"row\" fxLayoutGap=\"20px\" class=\"mat-body center-align\">\r\n\t\t\t\t\t<div>Number</div>\r\n\t\t\t\t\t<div>{{ model.number }}</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div fxLayout=\"row\" fxLayoutGap=\"20px\" class=\"mat-body center-align\">\r\n\t\t\t\t\t<div >Password</div>\r\n\t\t\t\t\t<div>{{ model.pwd}}</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<br>\r\n\t\t\t\t<button mat-raised-button  type=\"submit\" class=\"button-style\" (click)=\"submitted=false\">Edit</button>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/forms/template-driven-forms/template-driven-forms.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".form-header {\n  background-color: #327ebd;\n  color: white;\n  padding: 0 5px; }\n\n.mat-form-field {\n  padding-left: 10px; }\n\n.container {\n  padding-left: 10px;\n  padding-right: 10px; }\n\n.form-container {\n  padding: 5px 15px 15px 15px; }\n\n.form-component-holder {\n  border: 1px solid #327ebd;\n  background-color: white; }\n\n.button-style {\n  background-color: #327ebd;\n  font-weight: bold; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/forms/template-driven-forms/template-driven-forms.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TemplateDrivenFormsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__user__ = __webpack_require__("../../../../../src/app/forms/template-driven-forms/user.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TemplateDrivenFormsComponent = /** @class */ (function () {
    function TemplateDrivenFormsComponent() {
        this.model = new __WEBPACK_IMPORTED_MODULE_1__user__["a" /* User */](18, 'User', '', 8086824175);
        this.submitted = false;
    }
    TemplateDrivenFormsComponent.prototype.onSubmit = function () { this.submitted = true; };
    TemplateDrivenFormsComponent.prototype.newUser = function () {
        this.model = new __WEBPACK_IMPORTED_MODULE_1__user__["a" /* User */](42, '', ' ');
    };
    TemplateDrivenFormsComponent.prototype.ngOnInit = function () {
    };
    TemplateDrivenFormsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'cdk-template-driven-forms',
            template: __webpack_require__("../../../../../src/app/forms/template-driven-forms/template-driven-forms.component.html"),
            styles: [__webpack_require__("../../../../../src/app/forms/template-driven-forms/template-driven-forms.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], TemplateDrivenFormsComponent);
    return TemplateDrivenFormsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/forms/template-driven-forms/user.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return User; });
var User = /** @class */ (function () {
    function User(id, name, pwd, number) {
        this.id = id;
        this.name = name;
        this.pwd = pwd;
        this.number = number;
    }
    return User;
}());



/***/ })

});
//# sourceMappingURL=forms.module.chunk.js.map